package com.example.forum.utils;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
public class StringUtils {

    /**
     * 校验指定的字符串是否为空
     * @param value 待校验的字符串
     * @return true 空 <br/> false 非空
     */
    public static boolean isEmpty (String value) {
        if (value == null || value.isEmpty()) {
            return true;
        }
        return false;
    }
}
