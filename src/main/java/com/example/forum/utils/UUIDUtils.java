package com.example.forum.utils;

import java.util.UUID;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
public class UUIDUtils {

    /**
     * 生成一个标准的36字符的UUID
     * @return
     */
    public static String UUID_36 () {
        return UUID.randomUUID().toString();
    }

    /**
     * 生成一个32字符的UUID
     * @return
     */
    public static String UUID_32 () {
        return UUID.randomUUID().toString().replace("-", "");
    }

}
