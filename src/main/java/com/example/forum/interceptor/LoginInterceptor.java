package com.example.forum.interceptor;

import com.example.forum.config.AppConfig;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {
    // 从配置⽂件中获取默认登录⻚的URL
    // 从配置文件中读取配置内容
    @Value("${forum.login.url}")
    private String defaultURL;

    /**
     * 预处理(请求的前置处理)回调方法<br/>
     *
     * @return true 继续请求流程 </br> false 中止请求流程
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取session
        HttpSession session = request.getSession(false);
        // 判断session和session中保存的用户信息是否有效
        if (session != null && session.getAttribute(AppConfig.SESSION_USER_KEY) != null) {
            // 校验通过
            return true;
        }
        // 校验不通过时要处理的逻辑
        // 1. 返回一个错误的HTTP状态码
//        response.setStatus(403);
        // 2. 跳转到某一个页面
//        response.sendRedirect("/sign-in.html");
        // 对URL前缀做校验(确保目标URL从根目录开发)
        if (!defaultURL.startsWith("/")) {
            defaultURL = "/" + defaultURL;
        }
        response.sendRedirect(defaultURL);
        // 校验不能过
        return false;
    }
}

