package com.example.forum.dao;

import com.example.forum.model.ArticleReply;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ArticleReplyMapper {
    int insert(ArticleReply record);

    int insertSelective(ArticleReply record);

    ArticleReply selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ArticleReply record);

    int updateByPrimaryKey(ArticleReply record);

    /**
     * 查询帖⼦对应的回复
     * @param articleId 帖⼦Id
     * @return
     */
    List<ArticleReply> selectByArticleId (Long articleId);
}