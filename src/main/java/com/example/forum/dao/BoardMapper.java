package com.example.forum.dao;

import com.example.forum.model.Board;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BoardMapper {
    int insert(Board record);

    int insertSelective(Board record);

    Board selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Board record);

    int updateByPrimaryKey(Board record);

    /**
     * 查询首页的版块列表
     * @param num 要查询的数量
     * @return List<Board>
     */
    List<Board> selectByNum (@Param("num") Integer num);

}