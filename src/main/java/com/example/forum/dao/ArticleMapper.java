package com.example.forum.dao;

import com.example.forum.model.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleMapper {
    int insert(Article record);

    int insertSelective(Article record);

    Article selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Article record);

    int updateByPrimaryKeyWithBLOBs(Article record);

    int updateByPrimaryKey(Article record);

    /**
     * 查询所有的帖子集合
     * @return List<Article>
     */
    List<Article> selectAll ();

    /**
     * 根据版块Id查询帖子列表
     * @param boardId 版块Id
     * @return List<Article>
     */
    List<Article> selectByBoardId(@Param("boardId") Long boardId);

    /**
     * 根据帖子Id查询帖子详情
     * @param id 帖子Id
     * @return Article
     */
    Article selectById (@Param("id") Long id);

    int updateArticleMapperById(Long id);

    List<Article> selectByUserId (Long userId);

    List<Article> selectSort();

    List<Article> selectByTitle(String title);
}