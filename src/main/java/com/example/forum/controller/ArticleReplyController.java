package com.example.forum.controller;

import com.example.forum.common.AppResult;
import com.example.forum.common.ResultCode;
import com.example.forum.config.AppConfig;
import com.example.forum.exception.ApplicationException;
import com.example.forum.model.Article;
import com.example.forum.model.ArticleReply;
import com.example.forum.model.User;
import com.example.forum.service.IArticleReplyService;
import com.example.forum.service.IArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
@Api(tags = "回复接⼝")
@Slf4j
@RestController
@RequestMapping("/reply")
public class ArticleReplyController {

    @Resource
    private IArticleReplyService articleReplyService;

    @Resource
    private IArticleService articleService;

    /**
     * 回复帖⼦
     *
     * @param articleId 帖⼦Id
     * @param content   回复内容
     * @return AppResult
     */
    @ApiOperation("新建回复帖⼦")
    @PostMapping("/create")
    public AppResult create(HttpServletRequest request,
                            @ApiParam(value = "帖⼦Id") @RequestParam(value = "articleId") @NonNull Long articleId,
                            @ApiParam(value = "回复内容") @RequestParam(value = "content") @NonNull String content) {
        // 获取⽤⼾信息
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(AppConfig.SESSION_USER_KEY);
        // 判断是否被禁⾔
        if (user.getState() != 0) {
            // ⽇志
            log.warn(ResultCode.FAILED_USER_BANNED.toString() + ", userId = " + user.getId());
            // 返回错误信息
            return AppResult.failed(ResultCode.FAILED_USER_BANNED);
        }

        // 获取帖⼦信息
        Article article = articleService.selectById(articleId);
        // 校验帖⼦信息
        if (article == null) {
            log.warn("修改的帖⼦不存在. articleId = " + articleId);
            return AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE);
        }
        if (article.getState() != 0 || article.getDeleteState() != 0) {
            log.warn("帖⼦状态异常. articleId = " + articleId + ", state = " +
                    article.getState() + ", delete state = " + article.getDeleteState());
            return AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE);
        }

        // 构造数据
        ArticleReply articleReply = new ArticleReply();
        articleReply.setArticleId(articleId);
        articleReply.setPostUserId(user.getId());
        articleReply.setContent(content);
        articleReply.setReplyId(null);
        articleReply.setReplyUserId(null);
        // 调⽤业务层
        articleReplyService.create(articleReply);
        return AppResult.success();
    }

    /**
     * 回复帖⼦
     *
     * @param articleId 帖⼦Id
     * @return AppResult
     */
    @ApiOperation("回复帖⼦列表")
    @GetMapping("/getReplies")
    public AppResult<List<ArticleReply>> getRepliesByArticleId(@ApiParam(value = "帖⼦Id") @RequestParam(value = "articleId") @NonNull Long articleId) {
            // 查询帖⼦是否存在
            Article article = articleService.selectById(articleId);
            if (article == null || article.getDeleteState() == 1) {
                // 记录⽇志
                log.info(ResultCode.FAILED_PARAMS_VALIDATE.toString());
                // 抛出异常
                throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
            }
            // 查询结果
            List<ArticleReply> result = articleReplyService.selectByArticleId(articleId);
            // 返回结果
            return AppResult.success(result);
}
}
