package com.example.forum.controller;

import com.example.forum.common.AppResult;
import com.example.forum.common.ResultCode;
import com.example.forum.config.AppConfig;
import com.example.forum.model.Article;
import com.example.forum.model.User;
import com.example.forum.service.IArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
@Api(tags = "帖子接口")
@Slf4j
@RestController
@RequestMapping("/article")
public class ArticleController {

    @Resource
    private IArticleService articleService;

    @ApiOperation("获取版块帖子列表")
    @GetMapping("/getAllByBoardId")
    public AppResult<List<Article>> getAllByBoardId (@ApiParam("版块Id") @RequestParam(value = "boardId", required = false) Long boardId) {
        // 声明返回的集合
        List<Article> articles;
        // 根据传入的boardId来获取不同的集合
        if (boardId == null) {
            // 首页，查询所有
            articles = articleService.selectAll();
        } else if(boardId == 10){
            articles = articleService.selectSort();
        }else {
            // 查询对应版块中的帖子集合
            articles = articleService.selectByBoardId(boardId);
        }

        // 判断是否为空
        if (articles == null) {
            articles = new ArrayList<>();
        }
        // 返回结果
        return AppResult.success(articles);
    }

    @ApiOperation("发布帖子")
    @PostMapping("/create")
    public AppResult create (HttpServletRequest request,
                             @ApiParam("版块Id") @RequestParam("boardId") @NonNull Long boardId,
                             @ApiParam("帖子标题") @RequestParam("title") @NonNull String title,
                             @ApiParam("帖子正文") @RequestParam("content") @NonNull String content) {
        // 获取用户信息
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute(AppConfig.SESSION_USER_KEY);
        // 校验用户状态
        if (user.getState() == 1) {
            // 用户已禁言, 返回提示
            return AppResult.failed(ResultCode.FAILED_USER_BANNED);
        }
        // 构造帖子对象
        Article article = new Article();
        article.setUserId(user.getId()); // 当前登录用户就是作者
        article.setBoardId(boardId); // 版块Id
        article.setTitle(title); // 帖子标题
        article.setContent(content); // 帖子正文
        // 调用Service
        articleService.create(article);
        // 返回结果
        return AppResult.success("帖子发布成功");
    }

    /**
     * 根据Id查询帖⼦详情
     *
     * @param id 版块Id
     * @return 指定Id的帖⼦详情
     */
    @ApiOperation("获取帖子详情")
    @GetMapping("/getById")
    public AppResult<Article> getDetails (HttpServletRequest request,
                                                @ApiParam("帖子Id") @RequestParam("id") @NonNull Long id) {
        // 获取⽤⼾信息
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(AppConfig.SESSION_USER_KEY);
        // 调用Service
        Article article = articleService.selectById(id);
        // 校验
        if (article == null) {
            // 返回提示
            return AppResult.failed("帖子不存在");
        }
        if (user.getId() == article.getUserId()) {
            article.setOwn(true);
        }
        // 返回成功信息
        return AppResult.success(article);
    }

    /**
     * 根据⽤⼾Id查询帖⼦列表
     *
     * @param userId ⽤⼾Id
     * @return 指定版块的帖⼦列表
     */
    @ApiOperation("根据⽤⼾Id查询帖⼦列表")
    @GetMapping("/getAllByUserId")
    public AppResult<List<Article>> getAllByUserId(HttpServletRequest request,
                                                        @ApiParam(value = "⽤⼾Id") @RequestParam(value = "userId",required = false) Long userId) {
        List<Article> result = new ArrayList<>();
        if(userId == null){
            // 获取⽤⼾信息
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(AppConfig.SESSION_USER_KEY);
            result = articleService.selectByUserId(user.getId());
        }else {
            // 查询⽤⼾的帖⼦列表
            result = articleService.selectByUserId(userId);
            log.info("查询⽤⼾帖⼦列表, userId = " + userId);
        }
        // 返回结果
        return AppResult.success(result);
    }


    @ApiOperation("编辑帖⼦")
    @PostMapping("/modify")
    public AppResult modify(HttpServletRequest request,
                            @ApiParam(value = "帖⼦Id") @RequestParam(value = "id")
                            @NonNull Long id,
                            @ApiParam(value = "⽂章标题") @RequestParam(value =
                                    "title") @NonNull String title,
                            @ApiParam(value = "帖⼦内容") @RequestParam(value =
                                    "content") @NonNull String content) {
        // 获取⽤⼾信息
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(AppConfig.SESSION_USER_KEY);
        // 判断是否被禁⾔
        if (user.getState() != 0) {
            // ⽇志
            log.warn(ResultCode.FAILED_USER_BANNED.toString() + ", userId = " +
                    user.getId());
            // 返回错误信息
            return AppResult.failed(ResultCode.FAILED_USER_BANNED);
        }
        // 获取帖⼦原始数据
        Article article = articleService.selectById(id);
        // 数据校验
        if (article == null) {
            log.warn("修改的帖⼦不存在. articleId = " + id);
            return AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE);
        }
        // 发帖⼈不是当前⽤⼾
        if (article.getUserId() != user.getId()) {
            log.warn(MessageFormat.format("发帖⼈不是当前⽤⼾. article id = {0}, article user id = {1}, " +
                    "current user id = {2}", article.getId(), article.getUserId(),
                    user.getId()));
            return AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE);
        }
        // 帖⼦状态异常
        if (article.getState() != 0 || article.getDeleteState() != 0) {
            log.warn("帖⼦状态异常. articleId = " + id + ", state = " +
                    article.getState() + ", delete state = " + article.getDeleteState());
            return AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE);
        }
        // 调⽤业务层
        articleService.modify(id, title, content);
        // ⽇志
        log.info(MessageFormat.format("帖⼦修改成功. id = {0}, user id = {1}, 原标题 = {2}, 新标题 = {3}",
                id, user.getId(), article.getTitle(), title));
        // 返回结果
        return AppResult.success();
    }

    /**
     * 点赞
     *
     * @param id 帖⼦Id
     * @return AppResult
     */
    @ApiOperation("点赞")
    @PostMapping("/thumbsUp")
    public AppResult thumbsUp(HttpServletRequest request,
                              @ApiParam(value = "帖⼦Id") @RequestParam(value = "id") @NonNull Long id) {
        // 获取⽤⼾信息
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(AppConfig.SESSION_USER_KEY);
        // 判断是否被禁⾔
        if (user.getState() != 0) {
            // ⽇志
            log.warn(ResultCode.FAILED_USER_BANNED.toString() + ", userId = " + user.getId());
            // 返回错误信息
            return AppResult.failed(ResultCode.FAILED_USER_BANNED);
        }
        // 更新点赞数
        articleService.thumbsUpById(id);
        // 返回结果
        return AppResult.success();
    }

    /**
     * 根据Id删除帖⼦
     *
     * @param id 帖⼦Id
     * @return
     */
    @ApiOperation("删除帖⼦")
    @PostMapping("/delete")
    public AppResult deleteById (HttpServletRequest request,
                                 @ApiParam("帖⼦Id") @RequestParam("id") @NonNull Long
                                         id) {
        // 1. ⽤⼾是否禁⾔
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute(AppConfig.SESSION_USER_KEY);
        if (user.getState() == 1) {
            // 返回错误描述
            return AppResult.failed(ResultCode.FAILED_USER_BANNED);
        }
        // 查询帖⼦信息
        Article article = articleService.selectById(id);
        // 2. 帖⼦是否存在或已删除
        if (article == null || article.getDeleteState() == 1) {
            // 返回错误描述
            return AppResult.failed(ResultCode.FAILED_NOT_EXISTS);
        }
        // 3. ⽤⼾是否是作者
        if (article.getUserId() != user.getId()) {
            // 返回错误描述
            return AppResult.failed(ResultCode.FAILED_FORBIDDEN);
        }
        // 调⽤Service执⾏删除
        articleService.deleteById(id);
        // 返回成功
        return AppResult.success();
    }

    /**
     * 根据title查询帖⼦列表
     *
     * @param title 帖子title
     * @return 指定title的帖⼦列表
     */
    @ApiOperation("根据title查询帖⼦列表")
    @GetMapping("/title")
    public AppResult<List<Article>> getDetails(HttpServletRequest request,
                                         @ApiParam(value = "帖⼦title")
                                         @RequestParam(value = "title") @NonNull String title) {
        // 调⽤Service层获取帖⼦详情
        List<Article> articles = articleService.selectByTitle(title);
        // 判断是否为空
        if (articles == null) {
            articles = new ArrayList<>();
        }
        // 返回结果
        return AppResult.success(articles);
    }
}
