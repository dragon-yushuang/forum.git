package com.example.forum.controller;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */

import com.example.forum.common.AppResult;
import com.example.forum.exception.ApplicationException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// API 描述信息
@Api(tags = "TestController 测试接⼝")
// 指定测试根路径映射地址前缀
@RequestMapping("/test")
// 返回JSON格式的Controller
@RestController()
//@RequestMapping("/test")
//// Controller注解，返回的对象⽤JSON形式表⽰
//@RestController
public class TestController {

        // API ⽅法描述
        @ApiOperation(value = "测试接⼝1，打印hello")
        // 地址映射
        @GetMapping("/hello")
        // 具体⽅法实现
        public String hello() {
            return "Hello, Spring Boot...";
        }
        // 注意：@ApiParam ⽅法参数的注解，⽤来描述参数
        @ApiOperation(value = "测试接⼝3，返回 你好+传⼊参数")
        @GetMapping("/helloByName")
        public AppResult<String> helloByName(@ApiParam(value = "名字", required =
                true) String name) {
            return AppResult.success("你好：" + name);
        }
        @ApiOperation(value = "测试接⼝2，跳转到index.html")
        @GetMapping("/index")
        public String index() {
            return "index.html";
        }
        @ApiOperation(value = "测试接⼝3，返回⼀个异常结果")
        @GetMapping("/exception")
        public String testException() throws Exception {
            throw new Exception("这是⼀个Exception");
        }

        @ApiOperation(value = "测试接⼝3，返回⼀个⾃定义异常结果")
        @GetMapping("/appException")
        public String testApplicationException() {
            throw new ApplicationException("这是⼀个⾃定义的ApplicationException");
        }


//    @GetMapping("/hello")
//    public String hello() {
//        return "Hello, Spring Boot...";
//    }
//    @GetMapping("/exception")
//    public String testException() throws Exception {
//        throw new Exception("这是⼀个Exception");
//    }
//    @GetMapping("/appException")
//    public String testApplicationException() {
//        throw new ApplicationException("这是⼀个⾃定义的ApplicationException");
//    }
}
