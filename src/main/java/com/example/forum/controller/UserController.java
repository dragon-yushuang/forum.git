package com.example.forum.controller;

import com.example.forum.common.AppResult;
import com.example.forum.common.ResultCode;
import com.example.forum.config.AppConfig;
import com.example.forum.model.User;
import com.example.forum.service.IUserService;
import com.example.forum.utils.MD5Utils;
import com.example.forum.utils.StringUtils;
import com.example.forum.utils.UUIDUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
@Api(tags = "用户接口")
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private IUserService userService;

    @ApiOperation("用户注册")
    @PostMapping("/register")
    public AppResult register (@ApiParam("用户名") @RequestParam("username") @NonNull String username,
                               @ApiParam("昵称") @RequestParam("nickname")  @NonNull String nickname,
                               @ApiParam("密码") @RequestParam("password")  @NonNull String password,
                               @ApiParam("确认密码") @RequestParam("passwordRepeat")  @NonNull String passwordRepeat) {
        // 校验密码是否一致
        if (!password.equals(passwordRepeat)) {
            // 返回错误信息
            return AppResult.failed(ResultCode.FAILED_TWO_PWD_NOT_SAME);
        }
        // 构造对象
        User user = new User();
        user.setUsername(username); // 用户名
        user.setNickname(nickname); // 昵称
        // 处理密码
        // 1. 生成盐
        String salt = UUIDUtils.UUID_32();
        // 2. 生成密码的密文
        String encryptPassword = MD5Utils.md5Salt(password, salt);
        // 3. 设置密码和盐
        user.setPassword(encryptPassword);
        user.setSalt(salt);

        // 调用Service
        userService.createNormalUser(user);
        // 返回结果
        return AppResult.success("注册成功");
    }

    @ApiOperation("用户登录")
    @PostMapping("login")
    @ResponseBody
    public AppResult login(HttpServletRequest request,
                           @ApiParam("用户名") @RequestParam("username") @NonNull String username,
                           @ApiParam("密码") @RequestParam("password")  @NonNull String password){
        // 调用Service
        User user = userService.login(username, password);
        // 在session中保存当前登录的用户信息
        // 1. 获取session对象
        HttpSession session = request.getSession(true);
        // 2. 把用户信息保存在Session中
        session.setAttribute(AppConfig.SESSION_USER_KEY, user);
        // 返回成功
        return AppResult.success("登录成功");
    }

    /**
     * 退出登录
     * @return AppResult
     */
    @ApiOperation("退出登录")
    @ResponseBody
    @GetMapping("/logout")
    public AppResult logout (HttpServletRequest request) {
        // 获取session对象
        HttpSession session = request.getSession(false);
        // 判断session是否有效
        if (session != null) {
            log.debug("注销成功：" + session.toString());
            // 销毁session对象
            session.invalidate();
        }
        // 返回结果
        return AppResult.success("注销成功");
    }

    @ApiOperation("获取用户信息")
    @ResponseBody
    @GetMapping("/info")
    public AppResult<User> getUserInfo (HttpServletRequest request,
                                        @ApiParam("用户id") @RequestParam(value = "id",required = false) Long id) {
        User user = null;
        if(id == null){
            // 获取session
            HttpSession session = request.getSession(false);
//        if (session == null || session.getAttribute(AppConfig.SESSION_USER_KEY) == null) {
//            return AppResult.failed("用户未登录");
//        }
            // 从session中获取用户对象
            user = (User) session.getAttribute(AppConfig.SESSION_USER_KEY);
        }else {
            user = userService.selectById(id);
        }
        // 返回结果
        return AppResult.success(user);
    }

    @ApiOperation("修改个⼈信息")
    @PostMapping("/modifyInfo")
    public AppResult modifyInfo (HttpServletRequest request,
                                 @ApiParam("⽤⼾名") @RequestParam(value = "username",required = false) String username,
                                 @ApiParam("昵称") @RequestParam(value = "nickname",required = false) String nickname,
                                 @ApiParam("性别") @RequestParam(value = "gender",required = false) Byte gender,
                                 @ApiParam("邮箱") @RequestParam(value = "email",required = false) String email,
                                 @ApiParam("电话号") @RequestParam(value = "phoneNum",required = false) String phoneNum,
                                 @ApiParam("个⼈简介") @RequestParam(value = "remark",required = false) String remark) {
        // 1. 接收参数
        // 2. 对参数做⾮空校验（全部都为空，则返回错误描述）
        if (StringUtils.isEmpty(username) && StringUtils.isEmpty(nickname)
                && StringUtils.isEmpty(email) && StringUtils.isEmpty(phoneNum)
                && StringUtils.isEmpty(remark) && gender == null) {
            // 返回错误信息
            return AppResult.failed("请输⼊要修改的内容");
        }
        // 从session中获取⽤⼾Id
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute(AppConfig.SESSION_USER_KEY);
        // 3. 封装对象
        User updateUser = new User();
        updateUser.setId(user.getId()); // ⽤⼾Id
        updateUser.setUsername(username); // ⽤⼾名
        updateUser.setNickname(nickname); // 昵称
        updateUser.setGender(gender); // 性别
        updateUser.setEmail(email); // 邮箱
        updateUser.setPhoneNum(phoneNum); // 电话
        updateUser.setRemark(remark); // 个⼈简介
        // 4. 调⽤Service中的⽅法
        userService.modifyInfo(updateUser);
        // 5. 查询最新的⽤⼾信息
        user = userService.selectById(user.getId());
        // 6. 把最新的⽤⼾信息设置到session中
        session.setAttribute(AppConfig.SESSION_USER_KEY, user);
        // 7. 返回结果
        return AppResult.success(user);
    }

    @ApiOperation("修改密码")
    @PostMapping("/modifyPwd")
    public AppResult modifyPassword (HttpServletRequest request,
                                     @ApiParam("原密码") @RequestParam("oldPassword") @NonNull String oldPassword,
                                     @ApiParam("新密码") @RequestParam("newPassword") @NonNull String newPassword,
                                     @ApiParam("确认密码") @RequestParam("passwordRepeat") @NonNull String passwordRepeat) {
        // 1. 校验新密码与确认密码是否相同
        if (!newPassword.equals(passwordRepeat)) {
            // 返回错误描述
            return AppResult.failed(ResultCode.FAILED_TWO_PWD_NOT_SAME);
        }
        // 2. 获取当前登录的⽤⼾信息
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute(AppConfig.SESSION_USER_KEY);
        // 3. 调⽤Service
        userService.modifyPassword(user.getId(), newPassword, oldPassword);
        // 4. 销毁session
        if (session != null) {
            session.invalidate();
        }
        // 5. 返回结果
        return AppResult.success();
    }
}
