package com.example.forum.controller;

import com.example.forum.common.AppResult;
import com.example.forum.model.Board;
import com.example.forum.service.IBoardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
@Api(tags = "版块接口")
@Slf4j // 加入日志
@RestController
@RequestMapping("/board")
public class BoardController {

    // 从配置文件中获取主页中显示的版块个数，如果配置文件中没有做相应的配置：默认为9
    @Value("${forum.index.board-num:9}")
    private Integer indexBoardNum;

    @Resource
    private IBoardService boardService;

    @ApiOperation("获取首页版块列表")
    @GetMapping("/topList")
    public AppResult<List<Board>> topList () {
        log.debug("主页中显示的版块个数为：" + indexBoardNum);
        // 调用Service
        List<Board> result = boardService.selectByNum(indexBoardNum);
        // 返回结果
        return AppResult.success(result);
    }

    @ApiOperation("获取版块详情")
    @GetMapping("/getById")
    public AppResult<Board> getBoardInfo (@ApiParam("版块Id") @RequestParam("id") @NonNull Long id) {
        // 调用Service
        Board board = boardService.selectById(id);
        // 返回结果
        return AppResult.success(board);
    }
}
