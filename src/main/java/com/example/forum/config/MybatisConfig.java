package com.example.forum.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
@Configuration
// 指定Mybatis的扫描路径
@MapperScan("com.example.forum.dao")
public class MybatisConfig {
}
