package com.example.forum.config;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
public class AppConfig {

    /**
     * 为Session中存的User对象定义KEY
     */
    public static final String SESSION_USER_KEY = "SESSION_USER_KEY";
}
