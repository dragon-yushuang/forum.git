package com.example.forum.service;

import com.example.forum.model.Article;
import com.example.forum.model.ArticleReply;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IArticleService {
    /**
     * 查询所有的帖子集合
     * @return List<Article>
     */
    List<Article> selectAll ();

    /**
     * 根据版块Id查询帖子列表
     * @param boardId 版块Id
     * @return List<Article>
     */
    List<Article> selectByBoardId(Long boardId);

    /**
     * 发布新帖
     * @param article
     */
    @Transactional
    void create (Article article);

    /**
     * 根据帖子Id查询帖子详情
     * @param id 帖子Id
     * @return Article
     */
    Article selectById (Long id);

    List<Article> selectByUserId (Long userId);

    /**
     * 根据帖⼦Id更新帖⼦标题与内容
     * @param id 帖⼦Id
     * @param title 标题
     * @param content 内容
     */
    void modify(Long id, String title, String content);

    /**
     * 点赞
     * @param id 帖⼦Id
     */
    void thumbsUpById (Long id);

    /**
     * 根据Id删除帖⼦
     * @param id 帖⼦Id
     */
    @Transactional // 事务管理
    void deleteById(Long id);

    List<Article> selectSort();

    /**
    * @Description: 根据输入的title模糊查询
    * @Param: title 帖子标题
    */

    List<Article> selectByTitle(String title);
}
