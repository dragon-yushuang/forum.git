package com.example.forum.service;

import com.example.forum.model.Board;

import java.util.List;

public interface IBoardService {
    /**
     * 查询首页的版块列表
     * @param num 要查询的数量
     * @return List<Board>
     */
    List<Board> selectByNum (Integer num);

    /**
     * 根据Id查询版块信息
     * @param id 版块Id
     * @return Board
     */
    Board selectById (Long id);

    /**
     * 帖子数加
     * @param id
     * @return
     */
    void addOneArticleCountById(Long id);

    /**
     * 版块中的帖⼦数量 -1
     * @param id 版块Id
     */
    void subOneArticleCountById (Long id);
}
