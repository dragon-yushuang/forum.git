package com.example.forum.service.impl;

import com.example.forum.common.AppResult;
import com.example.forum.common.ResultCode;
import com.example.forum.dao.MessageMapper;
import com.example.forum.exception.ApplicationException;
import com.example.forum.model.Message;
import com.example.forum.model.User;
import com.example.forum.service.IMessageService;
import com.example.forum.service.IUserService;
import com.example.forum.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
@Slf4j
@Service
public class MessageServiceImpl implements IMessageService {
    @Resource
    private MessageMapper messageMapper;
    @Resource
    private IUserService userService;
    @Override
    public void create(Message message) {
        // ⾮空校验
        if (message == null || message.getPostUserId() == null
                || message.getReceiveUserId() == null
                || StringUtils.isEmpty(message.getContent())) {
            // 打印⽇志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 校验接收者是否存在
        User user = userService.selectById(message.getReceiveUserId());
        if (user == null || user.getDeleteState() == 1) {
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 设置默认值
        message.setState((byte) 0); // 表⽰未读状态
        message.setDeleteState((byte) 0);
        // 设置创建与更新时间
        Date date = new Date();
        message.setCreateTime(date);
        message.setUpdateTime(date);
        // 调⽤DAO
        int row = messageMapper.insertSelective(message);
        if (row != 1) {
            log.warn(ResultCode.FAILED_CREATE.toString());
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_CREATE));
        }
    }

    @Override
    public Integer selectUnreadCount(Long userId) {
        // ⾮空校验
        if (userId == null) {
            // 记录⽇志
            log.info(ResultCode.ERROR_IS_NULL.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_IS_NULL));
        }
        // 查询结果
        Integer result = messageMapper.selectUnreadCount(userId);
        return result;
    }

    @Override
    public List<Message> selectByReceiveUserId(Long receiveUserId) {
        // ⾮空校验
        if (receiveUserId == null) {
            // 记录⽇志
            log.info(ResultCode.ERROR_IS_NULL.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_IS_NULL));
        }
        // 调⽤DAO
        List<Message> result = messageMapper.selectByReceiveUserId(receiveUserId);
        // 返回结果
        return result;
    }

    @Override
    public Message selectById(Long id) {
        // ⾮空校验
        if (id == null) {
            // 记录⽇志
            log.info(ResultCode.ERROR_IS_NULL.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_IS_NULL));
        }
        // 查询结果
        Message result = messageMapper.selectByPrimaryKey(id);
        // 返回结果
        return result;
    }

    @Override
    public void updateStateById(Long id, Byte state) {
        // ⾮空校验
        if (id == null) {
            // 记录⽇志
            log.info(ResultCode.ERROR_IS_NULL.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_IS_NULL));
        }
        Message message = new Message();
        message.setId(id);
        message.setState(state);
//        更新状态
        messageMapper.updateByPrimaryKeySelective(message);
    }

    @Override
    public void reply(Long repliedId, Message message) {
        // ⾮空校验
        if (repliedId == null || repliedId <= 0) {
            // 打印⽇志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 校验repliedId对应的站内信状态
        Message existsMessage = messageMapper.selectByPrimaryKey(repliedId);
        if (existsMessage == null || existsMessage.getDeleteState() == 1) {
            // 打印⽇志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 更新状态为已回复
        updateStateById(repliedId, (byte) 2);
        // 回复的内容写⼊数据库
        create(message);
    }

}
