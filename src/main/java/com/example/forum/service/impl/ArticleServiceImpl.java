package com.example.forum.service.impl;

import com.example.forum.common.AppResult;
import com.example.forum.common.ResultCode;
import com.example.forum.dao.ArticleMapper;
import com.example.forum.dao.ArticleReplyMapper;
import com.example.forum.exception.ApplicationException;
import com.example.forum.model.Article;
import com.example.forum.model.ArticleReply;
import com.example.forum.service.IArticleReplyService;
import com.example.forum.service.IArticleService;
import com.example.forum.service.IBoardService;
import com.example.forum.service.IUserService;
import com.example.forum.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
@Slf4j
@Service
public class ArticleServiceImpl implements IArticleService {

    @Resource
    ArticleMapper articleMapper;
    @Resource
    private IUserService userService;
    @Resource
    private IBoardService boardService;

    @Override
    public List<Article> selectAll() {
        // 调用DAO
        List<Article> result = articleMapper.selectAll();
        // 返回结果
        return result;
    }

    @Override
    public List<Article> selectByBoardId(Long boardId) {
        // 非空校验
        if (boardId == null || boardId <= 0) {
            // 打印日志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 调用DAO
        List<Article> result = articleMapper.selectByBoardId(boardId);
        // 返回结果
        return result;
    }

    @Override
    public void create(Article article) {
        // 非空校验
        if (article == null || article.getBoardId() == null || article.getBoardId() <= 0
                || article.getUserId() == null || article.getUserId() <= 0
                || StringUtils.isEmpty(article.getTitle())
                || StringUtils.isEmpty(article.getContent())) {
            // 打印日志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 设置默认值
        article.setVisitCount(0); // 访问数量
        article.setReplyCount(0); // 回复数量
        article.setLikeCount(0); // 点赞数量
        article.setState((byte) 0); // 状态
        article.setDeleteState((byte) 0); // 是否删除
        Date date = new Date();
        article.setCreateTime(date); // 创建时间
        article.setUpdateTime(date); // 更新时间
        // 调用DAO
        int articleRow = articleMapper.insertSelective(article);
        if (articleRow != 1) {
            // 打印日志
            log.warn(ResultCode.FAILED_CREATE.toString() + " 新增帖子失败");
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_CREATE));
        }

        // 更新用户的帖子数
        userService.addOneArticleCountById(article.getUserId());
        // 更新版块的帖子数
        boardService.addOneArticleCountById(article.getBoardId());
        // 打印日志
        log.info(ResultCode.SUCCESS.toString() + " 帖子发布成功, articleId = " + article.getId());
    }

    @Override
    public Article selectById(Long id) {
        // 非空校验
        if (id == null || id <= 0) {
            // 打印日志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 调用DAO
        articleMapper.updateArticleMapperById(id);
        Article article = articleMapper.selectById(id);
        // 返回结果
        return article;
    }

    @Override
    public List<Article> selectByUserId(Long userId) {
        // ⾮空校验
        if (userId == null) {
            // 记录⽇志
            log.info(ResultCode.ERROR_IS_NULL.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_IS_NULL));
        }
        // 调⽤DAO层查询结果
        List<Article> result = articleMapper.selectByUserId(userId);
        // 返回结果
        return result;
    }

    public void modify(Long id, String title, String content) {
        // ⾮空校验
        if (id == null || StringUtils.isEmpty(title) ||
                StringUtils.isEmpty(content)) {
            // 记录⽇志
            log.info(ResultCode.ERROR_IS_NULL.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_IS_NULL));
        }
        // 构造帖⼦对象
        Article update = new Article();
        update.setId(id);
        update.setTitle(title);
        update.setContent(content);
        update.setUpdateTime(new Date());
        int row = articleMapper.updateByPrimaryKeySelective(update);
        if (row != 1) {
            // 记录⽇志
            log.info(ResultCode.ERROR_SERVICES.toString() + "articleId = " + id);
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_SERVICES));
        }
    }

    @Override
    public void thumbsUpById(Long id) {
        // ⾮空校验
        if (id == null || id <= 0) {
            // 记录⽇志
            log.info(ResultCode.ERROR_IS_NULL.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_IS_NULL));
        }
        // 查询帖⼦信息
        Article article = selectById(id);
        if (article == null || article.getState() == 1 || article.getDeleteState()
                == 1) {
            // FAILED_NOT_EXISTS
            // 记录⽇志
            log.info(ResultCode.FAILED_NOT_EXISTS.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_NOT_EXISTS));
        }
        // 构造更新对象
        Article update = new Article();
        update.setId(article.getId());
        update.setLikeCount(article.getLikeCount() + 1);
        // 更新数据库
        int row = articleMapper.updateByPrimaryKeySelective(update);
        if (row != 1) {
            // 记录⽇志
            log.info(ResultCode.FAILED_CREATE.toString() + "userId = " + article.getUserId());
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_CREATE));
        }
    }

    @Override
    public void deleteById(Long id) {
        // ⾮空校验
        if (id == null || id <= 0) {
            // 打印⽇志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 根据Id查询帖⼦信息
        Article article = articleMapper.selectByPrimaryKey(id);
        if (article == null || article.getDeleteState() == 1) {
            // 打印⽇志
            log.warn(ResultCode.FAILED_BOARD_NOT_EXISTS.toString() + ", article id = " + id);
                    // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_BOARD_NOT_EXISTS));
        }
        // 构造⼀个更新对象
        Article updateArticle = new Article();
        updateArticle.setId(article.getId());
        updateArticle.setDeleteState((byte) 1);
        // 调⽤DAO
        int row = articleMapper.updateByPrimaryKeySelective(updateArticle);
        if (row != 1) {
            // 打印⽇志
            log.warn(ResultCode.ERROR_SERVICES.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_SERVICES));
        }
        // 更新版块中的帖⼦数量
        boardService.subOneArticleCountById(article.getBoardId());
        // 更新⽤⼾发帖数
        userService.subOneArticleCountById(article.getUserId());
        log.info("删除帖⼦成功, article id = " + article.getId() + ", user id = " + article.getUserId() + ".");
    }

    @Override
    public List<Article> selectSort() {
        List<Article> articles = articleMapper.selectSort();
        log.info(articles.toString());
        return articles;
    }

    @Override
    public List<Article> selectByTitle(String title) {
        if(StringUtils.isEmpty(title)){
            // 打印⽇志
            log.warn(ResultCode.ERROR_SERVICES.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_SERVICES));
        }
        title = "%" + title + "%";
        List<Article> articles = articleMapper.selectByTitle(title);
        return articles;
    }


}