package com.example.forum.service.impl;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */

import com.example.forum.common.AppResult;
import com.example.forum.common.ResultCode;
import com.example.forum.dao.ArticleMapper;
import com.example.forum.dao.ArticleReplyMapper;
import com.example.forum.exception.ApplicationException;
import com.example.forum.model.Article;
import com.example.forum.model.ArticleReply;
import com.example.forum.service.IArticleReplyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 帖⼦回复相关的业务实现
 */
// ⽇志注解
@Slf4j
// 加⼊到Spring容器
@Service
public class ArticleReplyServiceImpl implements IArticleReplyService {
    // 注⼊DAO层
    @Resource
    private ArticleReplyMapper articleReplyMapper;
    @Resource
    private ArticleMapper articleMapper;
    @Override
    public void create(ArticleReply articleReply) {
        // ⾮空校验
        if (articleReply == null || articleReply.getArticleId() == null) {
            // 记录⽇志
            log.info(ResultCode.ERROR_IS_NULL.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_IS_NULL));
        }
        // 查询帖⼦信息
        Article article = articleMapper.selectByPrimaryKey(articleReply.getArticleId());
        // 校验帖⼦信息
        if (article == null) {
            // 记录⽇志
            log.info(ResultCode.FAILED_PARAMS_VALIDATE.toString() + ". articleId = " + articleReply.getArticleId());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_CREATE));
        }
        if (article.getState() != 0 || article.getDeleteState() != 0) {
            log.warn("帖⼦状态异常. articleId = " + article.getId() + ", state = " + article.getState() + ", delete state = " + article.getDeleteState());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 填充默认数据
        articleReply.setState((byte) 0); // 状态
        articleReply.setDeleteState((byte) 0); // 是否状态
        articleReply.setLikeCount(0); // 点赞数量
        // 时间
        Date date = new Date();
        articleReply.setCreateTime(date); // 创建时间
        articleReply.setUpdateTime(date); // 更新时间
        // 写⼊回复数据
        int row = articleReplyMapper.insertSelective(articleReply);
        if (row != 1) {
            // 记录⽇志
            log.info("新增帖⼦回复失败, userId = " + articleReply.getPostUserId() + ", articleId = " + articleReply.getArticleId());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_CREATE));
        }
        // 更新帖⼦回复数
        Article updateArticle = new Article();
        updateArticle.setId(article.getId());
        updateArticle.setReplyCount(article.getReplyCount() + 1);
        articleMapper.updateByPrimaryKeySelective(updateArticle);
        if (row != 1) {
            // 记录⽇志
            log.info("帖⼦回复数量更新失败, userId = " + articleReply.getPostUserId() + ", articleId = " + articleReply.getArticleId());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_CREATE));
        }
        // ⽇志
        log.info("新增帖⼦回复成功, userId = " + articleReply.getPostUserId() + ", articleId = " + articleReply.getArticleId() + ", articleReplyId = " + articleReply.getId());
    }

    @Override
    public List<ArticleReply> selectByArticleId(Long articleId) {
        // ⾮空校验
        if (articleId == null || articleId <= 0) {
            // 记录⽇志
            log.info(ResultCode.ERROR_IS_NULL.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_IS_NULL));
        }
        // 调⽤DAO层查询结果
        List<ArticleReply> result = articleReplyMapper.selectByArticleId(articleId);
        // 返回结果
        return result;
    }

}