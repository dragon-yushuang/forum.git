package com.example.forum.service.impl;

import com.example.forum.common.AppResult;
import com.example.forum.common.ResultCode;
import com.example.forum.dao.UserMapper;
import com.example.forum.exception.ApplicationException;
import com.example.forum.model.User;
import com.example.forum.service.IUserService;
import com.example.forum.utils.MD5Utils;
import com.example.forum.utils.UUIDUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
@Slf4j // 日志
@Service // 加入Spring
public class UserServiceImpl implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public User selectByName(String username) {
        // 非空校验
        if (StringUtils.isEmpty(username)) {
            // 打印日志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 根据用户名查询用户信息
        User user = userMapper.selectByName(username);
        // 返回结果
        return user;
    }

    @Override
    public void createNormalUser(User user) {
        // 非空校验
        if (user == null || StringUtils.isEmpty(user.getUsername())
                || StringUtils.isEmpty(user.getNickname()) || StringUtils.isEmpty(user.getPassword())
                || StringUtils.isEmpty(user.getSalt())) {
            // 打印日志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 校验用户名是否存在
        User existsUser = selectByName(user.getUsername());
        if (existsUser != null) {
            // 打印日志
            log.warn(ResultCode.FAILED_USER_EXISTS.toString() + " username = " + user.getUsername());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_USER_EXISTS));
        }
        // 为属性设置默认值
        // 性别
        if (user.getGender() != null) {
            if (user.getGender() < 0 || user.getGender() > 2) {
                user.setGender((byte) 2);
            }
        } else {
            user.setGender((byte) 2);
        }
        // 发帖数
        user.setArticleCount(0);
        // 是否管理员
        user.setIsAdmin((byte) 0);
        // 状态
        user.setState((byte) 0);
        // 是否删除
        user.setDeleteState((byte) 0);
        // 时间
        Date date = new Date();
        user.setCreateTime(date); // 创建时间
        user.setUpdateTime(date); // 更新时间

        // 写入数据库
        int row = userMapper.insertSelective(user);
        if (row != 1) {
            // 打印日志
            log.warn(ResultCode.FAILED_CREATE.toString() + " 注册用户失败. username = " + user.getUsername());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_CREATE));
        }
    }

    @Override
    public User login(String username, String password) {
        // 非空校验
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            // 打印日志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 根据用户名查询用户信息
        User user = selectByName(username);
        // 校验用户是否存在
        if (user == null) {
            // 打印日志
            log.info(ResultCode.FAILED_USER_NOT_EXISTS + ", username = " + username);
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_LOGIN));
        }
        // 校验密码是否正确
        String encryptPassword = MD5Utils.md5Salt(password, user.getSalt());
        if (!encryptPassword.equalsIgnoreCase(user.getPassword())) {
            // 打印日志
            log.info( "密码输入错误 , username = " + username + ", password = " + password);
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_LOGIN));
        }
        // 返回用户信息
        return user;
    }

    @Override
    public User selectById(Long id) {
        // 非空校验
        if (id == null || id < 0) {
            // 打印日志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 调用DAO查询
        User user = userMapper.selectByPrimaryKey(id);
        // 返回结果
        return user;
    }

    @Override
    public void addOneArticleCountById(Long id) {
// 非空校验
        if (id == null || id <= 0) {
            // 打印日志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 查询现有的用户信息
        User user = selectById(id);
        // 校验用户是否为空
        if (user == null) {
            // 打印日志
            log.warn(ResultCode.FAILED_USER_NOT_EXISTS.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_USER_NOT_EXISTS));
        }
        // 构造要更新的对象
//        User updateUser = new User();
//        updateUser.setId(user.getId()); // 用户Id
//        updateUser.setArticleCount(user.getArticleCount() + 1); // 帖子数量+1
//        updateUser.setUpdateTime(new Date()); // 更新时间
        // 调用DAO
//        int row = userMapper.updateByPrimaryKeySelective(updateUser);
        long row = userMapper.updateArticleCount(id);
        User users = selectById(id);
        if (row != 1) {
            // 打印日志
            log.warn(ResultCode.ERROR_SERVICES.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_SERVICES));
        }
    }

    @Override
    public void modifyInfo(User user) {
        // 1. ⾮空校验
        if (user == null || user.getId() == null || user.getId() <= 0) {
            // 打印⽇志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new
                    ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 2. 校验⽤⼾是否存在
        User existsUser = userMapper.selectByPrimaryKey(user.getId());
        if (existsUser == null) {
            // 打印⽇志
            log.warn(ResultCode.FAILED_USER_NOT_EXISTS.toString());
            // 抛出异常
            throw new
                    ApplicationException(AppResult.failed(ResultCode.FAILED_USER_NOT_EXISTS));
        }
        // 3. 定义⼀个标志位
        boolean checkAttr = false; // false表⽰没有校验通过
        // 4. 定义⼀个专⻔⽤来更新的对象，防⽌⽤⼾传⼊的User对象设置了其他的属性
        // 当使⽤动态SQL进⾏更新的时候，覆盖了没有经过校验的字段
        User updateUser = new User();
        // 5. 设置⽤⼾Id
        updateUser.setId(user.getId());
        // 6. 对每个⼀个参数进⾏校验并赋值
        if (!StringUtils.isEmpty(user.getUsername())
                && !user.getUsername().equals(existsUser.getUsername())) {
            // 需要更新⽤⼾名(登录名)时，进⾏唯⼀性的校验
            User checkUser = userMapper.selectByName(user.getUsername());
            if (checkUser != null) {
                // ⽤⼾已存在
                log.warn(ResultCode.FAILED_USER_EXISTS.toString());
                // 抛出异常
                throw new
                        ApplicationException(AppResult.failed(ResultCode.FAILED_USER_EXISTS));
            }
            // 数据库中没有找到相应的⽤⼾，表⽰可以修改⽤⼾名
            updateUser.setUsername(user.getUsername());
            // 更新标志位
            checkAttr = true;
        }
        // 7. 校验昵称
        if (!StringUtils.isEmpty(user.getNickname())
                && !user.getNickname().equals(existsUser.getNickname())) {
            // 设置昵称
            updateUser.setNickname(user.getNickname());
            // 更新标志位
            checkAttr = true;
        }
        // 8. 校验性别
        if (user.getGender() != null && user.getGender() !=
                existsUser.getGender()) {
            // 设置性别
            updateUser.setGender(user.getGender());
            // 合法性校验
            if (updateUser.getGender() > 2 || updateUser.getGender() < 0) {
                updateUser.setGender((byte) 2);
            }
            // 更新标志位
            checkAttr = true;
        }
        // 9. 校验邮箱
        if (!StringUtils.isEmpty(user.getEmail())
                && !user.getEmail().equals(existsUser.getEmail())) {
            // 设置邮箱
            updateUser.setEmail(user.getEmail());
            // 更新标志位
            checkAttr = true;
        }
        // 9. 校验电话号码
        if (!StringUtils.isEmpty(user.getPhoneNum())
                && !user.getPhoneNum().equals(existsUser.getPhoneNum())) {
            // 设置电话号码
            updateUser.setPhoneNum(user.getPhoneNum());
            // 更新标志位
            checkAttr = true;
        }
        // 10. 校验个⼈简介
        if (!StringUtils.isEmpty(user.getRemark())
                && !user.getRemark().equals(existsUser.getRemark())) {
            // 设置电话号码
            updateUser.setRemark(user.getRemark());
            // 更新标志位
            checkAttr = true;
        }
        // 11. 根据标志位来决定是否可以执⾏更新
        if (checkAttr == false) {
            // 打印⽇志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new
                    ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 12. 调⽤DAO
        int row = userMapper.updateByPrimaryKeySelective(updateUser);
        if (row != 1) {
            log.warn(ResultCode.FAILED.toString() + ", 受影响的⾏数不等于 1 .");
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED));
        }
    }

    @Override
    public void modifyPassword(Long id, String newPassword, String oldPassword) {
        // 1. 根据⽤⼾Id查询⽤⼾信息
        User user = userMapper.selectByPrimaryKey(id);
        // 2. 校验⽤⼾Id是否有存在
        if (user == null) {
            // 记录⽇志
            log.info(ResultCode.FAILED_UNAUTHORIZED.toString() + "user id = " +
                    id);
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_UNAUTHORIZED));
        }
        // 3. 校验原密码是否正确
        String oldEncryptPassword = MD5Utils.md5Salt(oldPassword, user.getSalt());
        if (!oldEncryptPassword.equalsIgnoreCase(user.getPassword())) {
            // 记录⽇志
            log.info(ResultCode.FAILED_PARAMS_VALIDATE.toString() + "username = "
                    + user.getUsername() + ", password = " + oldPassword);
            // 密码不正确抛出异常
            throw new
                    ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 4. 重新⽣成扰动字符串
        String salt = UUIDUtils.UUID_32();
        // 5. 计算新密码
        String encryptPassword = MD5Utils.md5Salt(newPassword, salt);
        // 6. 创建⽤于更新的User对象
        User modifyUser = new User();
        // 设置⽤⼾Id
        modifyUser.setId(user.getId());
        // 设置新密码
        modifyUser.setPassword(encryptPassword);
        // 设置扰动字符串
        modifyUser.setSalt(salt);
        // 设置更新时间
        modifyUser.setUpdateTime(new Date());
        // 7. 更新操作
        userMapper.updateByPrimaryKeySelective(modifyUser);
        // 打印⽇志
        log.info("⽤⼾密码修改成功：" + user.getUsername());
    }

    @Override
    public void subOneArticleCountById(Long id) {
        if (id == null || id <= 0) {
            // 打印⽇志
            log.warn(ResultCode.FAILED_PARAMS_VALIDATE.toString());
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED_PARAMS_VALIDATE));
        }
        // 查询⽤⼾信息
        User user = userMapper.selectByPrimaryKey(id);
        if (user == null) {
            // 打印⽇志
            log.warn(ResultCode.ERROR_IS_NULL.toString() + ", user id = " + id);
            // 抛出异常
            throw new ApplicationException(AppResult.failed(ResultCode.ERROR_IS_NULL));
        }
        // 更新⽤⼾的发帖数量
        User updateUser = new User();
        updateUser.setId(user.getId());
        updateUser.setArticleCount(user.getArticleCount() - 1);
        // 判断减1之后，⽤⼾的发帖数是否⼩于0
        if (updateUser.getArticleCount() < 0) {
            // 如果⼩于0，则设置为0
            updateUser.setArticleCount(0);
        }
        // 更新数据库
        int row = userMapper.updateByPrimaryKeySelective(updateUser);
        if (row != 1) {
            log.warn(ResultCode.FAILED.toString() + ", 受影响的⾏数不等于 1 .");
            throw new ApplicationException(AppResult.failed(ResultCode.FAILED));
        }
    }
}

