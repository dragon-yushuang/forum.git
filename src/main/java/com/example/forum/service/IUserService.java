package com.example.forum.service;

import com.example.forum.model.User;
import com.example.forum.utils.StringUtils;

public interface IUserService {

    /**
     * 根据用户名查询用户信息
     * @param username 用户名
     * @return
     */
    User selectByName (String username);

    /**
     * 创建普通用户
     * @param user 用户信息
     */
    void createNormalUser(User user);

    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     * @return
     */
    User login(String username, String password);


    /**
     * 根据Id查询用户信息
     * @param id 用户Id
     * @return User
     */
    User selectById (Long id);

    /**
     * 帖子数加
     * @param id
     * @return
     */
    void addOneArticleCountById(Long id);

    /**
    * 修改个人中心
    * @Param:
    * @Return:
    */

    void modifyInfo(User user);

    /**
     * 修改⽤⼾密码
     * @param id ⽤⼾Id
     * @param newPassword 新密码
     * @param oldPassword ⽼密码
     */
    void modifyPassword (Long id, String newPassword, String oldPassword);

    /**
     * ⽤⼾发帖数 -1
     * @param id 版块Id
     */
    void subOneArticleCountById (Long id);
}