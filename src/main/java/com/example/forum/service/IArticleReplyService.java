package com.example.forum.service;

import com.example.forum.model.ArticleReply;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
public interface IArticleReplyService {
    /**
     * 新增回复
     *
     * @param articleReply 回复信息
     */
    // 事务管理
    @Transactional
    void create(ArticleReply articleReply);

    List<ArticleReply> selectByArticleId (Long articleId);
}