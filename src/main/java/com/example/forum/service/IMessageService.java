package com.example.forum.service;

import com.example.forum.model.Message;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author longyushuang
 * @description: $
 * @param:
 * @return:
 * @date:
 */
public interface IMessageService {
    /**
     * 发送站内信息
     * @param message 站内信
     */
    void create (Message message);

    Integer selectUnreadCount (Long userId);

    /**
     * 根据接收者Id查询所有站内信
     * @param receiveUserId
     * @return
     */
    List<Message> selectByReceiveUserId (Long receiveUserId);

    /**
     * 读取站内信
     *
     * @param id 站内信Id
     * @return
     */
    Message selectById(Long id);
    /**
     * 根据Id更新
     * @param id,state
     * @return
     */
    void updateStateById(Long id, Byte state);

    /**
     * 回复站内信
     * @param repliedId 被回复的站内信Id
     * @param message 回复内容
     */
    @Transactional
    void reply (Long repliedId, Message message);
}