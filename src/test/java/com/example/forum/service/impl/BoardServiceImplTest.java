package com.example.forum.service.impl;

import com.example.forum.model.Board;
import com.example.forum.service.IBoardService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional  // 测试环境中事务注解可以加到类上
class BoardServiceImplTest {

    @Resource
    private IBoardService boardService;
    @Resource
    private ObjectMapper objectMapper;

    @Test
    void selectByNum() throws JsonProcessingException {
        List<Board> boards = boardService.selectByNum(9);
        System.out.println(objectMapper.writeValueAsString(boards));

    }

    @Test
    void selectById() throws JsonProcessingException {
        // JAVA
        Board board = boardService.selectById(1l);
        System.out.println(objectMapper.writeValueAsString(board));

        // C++
        board = boardService.selectById(2l);
        System.out.println(objectMapper.writeValueAsString(board));

        // 不存在
        board = boardService.selectById(100l);
        System.out.println(objectMapper.writeValueAsString(board));
    }

    @Test
    void addOneArticleCountById() {
        boardService.addOneArticleCountById(1l);
        System.out.println("更新成功");

        boardService.addOneArticleCountById(2l);
        System.out.println("更新成功");

    }

}