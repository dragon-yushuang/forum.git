package com.example.forum.service.impl;

import com.example.forum.model.Article;
import com.example.forum.service.IArticleService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ArticleServiceImplTest {

    @Resource
    private IArticleService articleService;
    @Resource
    private ObjectMapper objectMapper;

    @Test
    void selectAll() throws JsonProcessingException {
        List<Article> articles = articleService.selectAll();
        System.out.println(objectMapper.writeValueAsString(articles));
    }

    @Test
    void selectByBoardId() throws JsonProcessingException {
        // JAVA 版块
        List<Article> articles = articleService.selectByBoardId(1l);
        System.out.println(objectMapper.writeValueAsString(articles));

        // C++版块
        articles = articleService.selectByBoardId(2l);
        System.out.println(objectMapper.writeValueAsString(articles));

        // 不存在的版块
        articles = articleService.selectByBoardId(100l);
        System.out.println(objectMapper.writeValueAsString(articles));
    }

    @Test
    @Transactional
    void create() {
        Article article = new Article();
        article.setBoardId(9l);
        article.setUserId(1l);
        article.setTitle("单元测试标题1");
        article.setContent("单元测试内容1");
        // 调用Service
        articleService.create(article);
        System.out.println("写入成功");
    }

    @Test
    void selectById() throws JsonProcessingException {
        Article article = articleService.selectById(1l);
        System.out.println(objectMapper.writeValueAsString(article));

        article = articleService.selectById(8l);
        System.out.println(objectMapper.writeValueAsString(article));

        article = articleService.selectById(900l);
        System.out.println(objectMapper.writeValueAsString(article));
    }

    @Test
    void selectSort() throws JsonProcessingException {
        List<Article> articles = articleService.selectSort();
        System.out.println(objectMapper.writeValueAsString(articles));
    }

    @Test
    void selectByTitle() throws JsonProcessingException {
        List<Article> articles = articleService.selectByTitle("1");
        System.out.println(objectMapper.writeValueAsString(articles));
    }
}