package com.example.forum.service.impl;

import com.example.forum.model.User;
import com.example.forum.service.IUserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserServiceImplTest {
    @Resource
    private IUserService userService;

    @Resource
    private ObjectMapper objectMapper;
    @Test
    void selectByName() throws JsonProcessingException {
        // 调用Service查询用户信息
        User user = userService.selectByName("yuni");
        System.out.println(objectMapper.writeValueAsString(user));
        System.out.println("======================================");

//        user = userService.selectByName("bitboy111");
//        System.out.println(objectMapper.writeValueAsString(user));
//        System.out.println("======================================");
//
//        user = userService.selectByName(null);
//        System.out.println(objectMapper.writeValueAsString(user));
    }

    @Test
    void createNormalUser() {
        // 构造用户
        User user = new User();
        user.setUsername("TestUser3");
        user.setNickname("单元测试用户3");
        user.setPassword("123456");
        user.setSalt("123456");
        // 调用Service
        userService.createNormalUser(user);
        System.out.println("注册成功");
        System.out.println("=====================");


    }

    @Test
    void login() throws JsonProcessingException {
        // 正常用户
        User user = userService.login("yuni", "123456");
        System.out.println(objectMapper.writeValueAsString(user));

//        // 错误用户
//        user = userService.login("123456", "123456");
//        System.out.println(objectMapper.writeValueAsString(user));
    }

    @Test
    void selectById() throws JsonProcessingException {
        // bitboy
        User user = userService.selectById(1l);
        System.out.println(objectMapper.writeValueAsString(user));

        // bitgirl
        user = userService.selectById(1l);
        System.out.println(objectMapper.writeValueAsString(user));

        // null
        user = userService.selectById(20l);
        System.out.println(objectMapper.writeValueAsString(user));

    }

    @Test
//    @Transactional
        // 更新的测试，建议在方法级别上加入事务注解
    void addOneArticleCountById() {
        userService.addOneArticleCountById(1l);
        System.out.println("更新成功");

//        userService.addOneArticleCountById(10l);
//        System.out.println("更新成功");

    }
}